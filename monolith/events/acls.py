from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    pexel_headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state
    }
    pexel_url = "https://api.pexels.com/v1/search"
    pexel_response = requests.get(pexel_url, headers=pexel_headers, params=params)
    content = json.loads(pexel_response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}

def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geocode_params = {
        "q": city + "," + state + ",USA",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1
    }
    geocode_url = "http://api.openweathermap.org/geo/1.0/direct"
    geocode_response = requests.get(geocode_url, params=geocode_params)
    content = json.loads(geocode_response.content)
    lat = str(content[0]["lat"])
    lon = str(content[0]["lon"])
    current_weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    current_weather_url = "https://api.openweathermap.org/data/2.5/weather"

    current_weather_response = requests.get(
        current_weather_url, 
        params=current_weather_params
    )
    weather = json.loads(current_weather_response.content)
    try: return {
        "temp": (weather["main"]["temp"]),
        "description": weather["weather"][0]["description"]
    }
    except:
        return {
            "temp": None,
            "description": None,
        }
    