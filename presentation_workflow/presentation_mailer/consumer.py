import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()




def process_approval(ch, method, properties, body):
    #  "presenter_name": presentation.presenter_name,
    #           "presenter_email": presentation.presenter_email,
    #           "title": presentation.title,
    clue = json.loads(body)
    name = clue["presenter_name"]
    title = clue["title"]
    email = clue["presenter_email"]

    send_mail(
        "Your presentation has been accepted",
        f"{name}, we're happy to tell you that your presentation {title} has been accepted",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("Approval Email Sent")
    print("END CONSUMING")


def process_rejection(ch, method, properties, body):
    #  "presenter_name": presentation.presenter_name,
    #           "presenter_email": presentation.presenter_email,
    #           "title": presentation.title,
    clue = json.loads(body)
    name = clue["presenter_name"]
    title = clue["title"]
    email = clue["presenter_email"]

    send_mail(
        "Your presentation has been rejected",
        f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("Rejection Email Sent")
    print("END CONSUMING")


def main():
    # Set the hostname that we'll connect to
    parameters = pika.ConnectionParameters(host="rabbitmq")

    # Create a connection to RabbitMQ
    connection = pika.BlockingConnection(parameters)

    # Open a channel to RabbitMQ
    channel = connection.channel()

    # Create a queue if it does not exist
    channel.queue_declare(queue="presentation_approvals")
    channel.queue_declare(queue="presentation_rejections")

    # Configure the consumer to call the process_message function
    # when a message arrives
    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=process_approval,
        auto_ack=True,
    )
    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=process_rejection,
        auto_ack=True,
    )

    # Print a status
    print(" [*] Waiting for messages. To exit press CTRL+C")

    # Tell RabbitMQ that you're ready to receive messages
    channel.start_consuming()

while True:
    try:
        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError:
        print('Could not connect to RabbitMQ')
        time.sleep(2.0)